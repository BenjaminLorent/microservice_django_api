# Microservice Django

## NOTES

-[Python]("https://www.python.org/")

-[Django]("https://docs.djangoproject.com/fr/3.2/")

## Se placer dans un environnement virtuel

```cmd
py -3 -m venv venv
```

```cmd
venv\Scripts\activate
```

## Installer Django

```cmd
py -m pip install Django
```

installer django rest framework

```cmd
pip install djangorestframework 
```

## Installer la version de développement

```cmd
git clone https://github.com/django/django.git
```

```cmd
py -m pip install -e django\
```

faire un git pull à partir du répertoire django pour mettre à jour.

## dépendances nécessaires au projet **requirements.txt**

```txt
Django==4.0
djangorestframework==3.12.4
mysqlclient==2.0.3
django-mysql==3.12.0
django-cors-headers==3.8.0
pika==1.1.0
```