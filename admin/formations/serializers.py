from rest_framework import serializers
from .models import Formateur, Formation
#Serialisation de notre model(objet)
class FormationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Formation
        fields ='__all__'
class FormateurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Formateur
        fields ='__all__'
