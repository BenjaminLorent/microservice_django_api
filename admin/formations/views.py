from django.shortcuts import render
from rest_framework import serializers, viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Formateur, Formation
from .serializers import FormationSerializer
import random
# Create your views here.
#contient  les methodes CRUD
class FormationViewSet(viewsets.ViewSet):
    
    def list(self, request): #/api/formations list des formations GET request
        formations = Formation.objects.all()
        serializer = FormationSerializer(formations, many=True)
        return Response(serializer.data)

    def create(self, request): #/api/formations créer formations POST request
        serializer= FormationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    def retrieve(self, request, pk=None): #/api/formations/<str:id> retrouver une formation by ID des formations GET request
        formation = Formation.objects.get(id=pk)
        serializer= FormationSerializer(formation)
        return Response(serializer.data)

    def update(self, request, pk=None): #/api/formations/<str:id> update PUT request
        formation = Formation.objects.get(id=pk)
        serializer = FormationSerializer(instance=formation, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
    
    def destroy(self, request, pk=None): #/api/formations/<str:id> list des formations POST request
        formation = Formation.objects.get(id=pk)
        formation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class FormateurAPIView(APIView):
    def get(self, _):
        formateurs = Formateur.objects.all()
        formateur = random.choice(formateurs)
        return Response({
            'id': formateur.id,
            'nom': formateur.nom,
            'prenom': formateur.prenom
        })