from django.contrib import admin
from django.urls import path
from .views import FormateurAPIView, FormationViewSet

urlpatterns = [
    path('formations', FormationViewSet.as_view({
        'get': 'list',
        'post': 'create'
    })),
    path('formations/<str:pk>', FormationViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'delete': 'destroy',
    })),
    path('formateur', FormateurAPIView.as_view()
    
    ),
]